#!/bin/sh
O=$2"/o"
E=$2"/e"
R=$2"/r"
S=$2"/s"
mkdir -p $2
if [ $1 -eq 0 ]; then
echo -e $(echo $3 | sed 's/.\{2\}/\\x&/g') > $S
/bin/sh $S 1> $O 2> $E
echo -ne "$?\n"\
"$(cat $O | wc -l)\n"\
"$(cat $E | wc -l)\n"\
"$(cat $O)\n"\
"$(cat $E)" > $R
else
(${C}) 1> $O 2> $E &
echo -ne "$?\n$!" > $R
fi
