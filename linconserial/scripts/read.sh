#!/bin/sh
I=$(expr $3 \* $2 + 1)
N=$(hexdump -v -e '/1 "%02x"' $1 | cut -c $I-$(expr $I + $3 - 1) | tr -d "\n")
echo $N\:$(echo -n $N | md5sum | cut -d " " -f1)
