from setuptools import setup, find_packages

setup(
    name =           'LinconSerial',
    version =        '0.0.7',
    description =    'Linux Connect Over Serial',
    author =         'Sean Jennings',
    author_email =   'seanrjennings@gmail.com',
    license =        'MIT',
    url =            'https://bitbucket.org/sjennings/linconserial',
    download_url =   'https://bitbucket.org/sjennings/linconserial/get/master.tar.gz',
    classifiers =   ['Development Status :: 3 - Alpha',
                     'Programming Language :: Python :: 2.7'],
    keywords =      [],
    packages =       find_packages(),
    install_requires = [],
)
