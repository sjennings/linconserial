# What is LinconSerial?

"LinconSerial" (pronounced "Lincoln Serial") is an acronym for "(Lin)ux
(con)nect over (serial). This is a tool for interfacing with a linux device
over a serial connection such as a debug UART.